use std::{
    fs::{File, OpenOptions},
    io::{BufRead, BufReader, BufWriter, Write},
    path::PathBuf,
    str::FromStr,
};

use clap::Parser;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(value_name = "FILE", value_parser=files_validator, help = "One or more files to merge")]
    files: Vec<PathBuf>,

    #[arg(
        short,
        long,
        value_name = "FILE",
        help = "File to write result to, otherwise result will be output to stdout"
    )]
    output: Option<PathBuf>,

    #[arg(
        short,
        long,
        requires = "output",
        help = "Whether to override existing output file"
    )]
    force: bool,
}

fn main() {
    let cli = Args::parse();

    match cli.files.len() {
        0 => {
            eprintln!("At least one file is required");
            std::process::exit(exitcode::USAGE);
        }
        1 => {
            let f = match File::open(cli.files.first().unwrap()) {
                Ok(f) => f,
                Err(_) => {
                    eprintln!("Could not open file");
                    std::process::exit(exitcode::IOERR);
                }
            };
            let mut reader = BufReader::new(f);

            match cli.output {
                Some(out_file) => {
                    if out_file.exists() && !cli.force {
                        eprintln!(
                            "File \"{}\" already exists. Use --force to overwrite.",
                            out_file.to_str().unwrap()
                        );
                        std::process::exit(exitcode::USAGE);
                    }

                    let of = match OpenOptions::new().write(true).truncate(true).open(out_file) {
                        Ok(of) => of,
                        Err(_) => {
                            eprintln!("Could not open output file");
                            std::process::exit(exitcode::IOERR);
                        }
                    };
                    let mut writer = BufWriter::new(of);
                    if std::io::copy(&mut reader, &mut writer).is_err() {
                        eprintln!("Could not write output file");
                        std::process::exit(exitcode::IOERR);
                    };
                    return;
                }
                None => {
                    let mut line = String::new();
                    while reader.read_line(&mut line).unwrap() > 0 {
                        println!("{line}");
                    }
                    return;
                }
            }
        }
        _ => {}
    }

    let mut errors = vec![];
    let toml_files = cli
        .files
        .iter()
        .map(std::fs::read_to_string)
        .filter_map(|r| r.map_err(|e| errors.push(e.to_string())).ok())
        .collect::<Vec<_>>()
        .iter()
        .map(|fd| toml::Table::from_str(&fd[..]))
        .filter_map(|r| r.map_err(|e| errors.push(e.to_string())).ok())
        .collect::<Vec<_>>();

    if !errors.is_empty() {
        eprintln!("Error reading source file:");
        for error in errors {
            eprintln!("- {error}");
        }
        std::process::exit(exitcode::DATAERR);
    }

    let mut output = toml::Table::new();
    for map in toml_files {
        merge_table(&mut output, &map);
    }

    let output = match toml::to_string(&output) {
        Ok(t) => t,
        Err(_) => {
            eprintln!("Failed serializing TOML Result");
            std::process::exit(exitcode::SOFTWARE);
        }
    };
    match cli.output {
        Some(of) => {
            if of.exists() && !cli.force {
                eprintln!(
                    "File \"{}\" already exists. Use --force to overwrite.",
                    of.to_str().unwrap()
                );
                std::process::exit(exitcode::USAGE);
            }

            let of = match OpenOptions::new().write(true).create(true).open(of) {
                Ok(of) => of,
                Err(_) => {
                    eprintln!("Could not open output file");
                    std::process::exit(exitcode::IOERR);
                }
            };
            let mut writer = BufWriter::new(of);
            if writer.write_all(output.as_bytes()).is_err() {
                eprintln!("Could not write output file");
                std::process::exit(exitcode::IOERR);
            }
        }
        None => {
            println!("{output}");
        }
    }

    std::process::exit(exitcode::OK);
}

fn merge_table(target: &mut toml::Table, source: &toml::Table) {
    for (key, value) in source {
        match target.get_mut(key) {
            Some(target_value) => {
                if !target_value.same_type(value) {
                    target.insert(key.clone(), value.clone());
                    continue;
                }

                match target_value {
                    toml::Value::Table(target_table) => {
                        merge_table(target_table, value.as_table().unwrap())
                    }
                    toml::Value::Array(target_array) => {
                        value.as_array().unwrap().clone_into(target_array);
                    }
                    _ => {
                        target.insert(key.clone(), value.clone());
                    }
                }
            }
            None => {
                target.insert(key.clone(), value.clone());
            }
        }
    }
}

fn files_validator(s: &str) -> Result<PathBuf, String> {
    let md = std::fs::metadata(s).map_err(|e| e.to_string())?;
    if md.is_dir() {
        return Err("Path must be a file".to_string());
    }

    let pb = PathBuf::from_str(s).map_err(|_| "Could not resolve file")?;
    if let Some(ext) = pb.extension() {
        if ext
            .to_str()
            .unwrap_or_default()
            .eq_ignore_ascii_case("toml")
        {
            return Ok(pb);
        }
    }
    Err("Only TOML files supported".to_string())
}
